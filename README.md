# Feel your Feed

Using sentiment analysis of tweets to drive haptic motor feedback

## Table of contents
* [General Info](#general-info)
* [Skills and Materials](#skills-and-materials)
* [Setting up your Raspberry Pi](#setting-up-your-raspberry-pi)
* [Libraries and Settings](#libraries-and-settings)
* [Physical Computing](#pyhsical-computing)
* [Access to Twitter's API](#access-to_twitter's-api)
* [Sentiment Analysis](#sentiment-analysis)
* [Haptic Controler](#haptic-controler)
* [Script and Execcution](#Script-and-Execution)


## General info
This project was created as a part of my thesis work for the Masters of Design in Emergent Futures Program at the Institute of Advanced Architecture Catalonia, in collaboration with ELISAVA Design school, Universitat Polytechnica de Catalunia, & Fab Lab Barcelona. 
Its part of a broader project focusing on designing the inputs of our realities. 
More information on that project and the theory behind the work can be found here (inser link)

<img src="Images/gif.gif" width="550">
	
## Skills and Materials
Basic Skills:
* use of Terminal in mac or windows
* basic level python3 coding
* soldering (through hole)

Materials
* Raspberry Pi - wifi model prefered - i'm using a 3b+ (non wifi Pi will require an alternate means of setup)
* Haptic Motor Controllr board with a Texas Instruments 2605L controler - either the Sparkfun or Adafruit versions will work 
* 2v-5v coin haptic motor
* access to a soldering iron
* jumper cables/breadboard
	

## Setting up your Raspberry Pi
For this project I used a new Raspberri Pi 3b+ which required a fair amout of configuration to get it up and running. As I did no have access to a spare keyboard and monitor, I opted to SSH into the pi using my Macbook and later used a the app VCN viewer to remote-desktop into the Pi.
I found a great tutorial from github user @calebbrewer which walks you through all the steps using your mac Terminal. 
#Headless Setup of Raspberry Pi Zero W (Raspberry Pi 3 Wireless) (macOS) 

1. **Formatt the Micro SD card** - Open a terminal and type 'diskutil list'. Find your card and copy the disk name (For example: /dev/disk4). Format the card with `diskutil eraseDisk ExFat temp disk4(Use your disk here)`
2. **Download Raspbian** - `wget https://downloads.raspberrypi.org/raspbian_lite_latest`
3. **Unmount the SD card** - `diskutil unmountDisk /dev/disk4` or whatever your disk path is
4. **Mount the Raspbian image to the card** - `sudo dd if=PATH-TO-RASPBIAN-IMAGE` of=/dev/disk4` or whatever your disk path is (I chose not to do this through raspberry pi imager which can be found here https://www.raspberrypi.org/documentation/installation/installing-images/ but if you prefer terminal instructuions the are here)
5. **Enable SSH on the Pi** - `cd /volumes && ls`. You should see a boot partition from the SD card `cd boot && touch ssh`
6. **Setup WiFi on the PI** -  While still in the boot partition of the card type `nano wpa_supplicant.conf` and enter `network={ ssid="YOUR-SSID" psk="YOUR-WIFI-PASSWORD" }`
7. **Boot the PI** - Unmount the card `diskutil unmountDisk /dev/disk4` (or whatever your disk path is) and put it in the Pi, then power up the Pi
8. **SSH Into the Pi** - Find the Pi's IP on your network by running `arp -a` or using an app like LanScan and ssh into it `ssh pi@YOUR_PIS-IP`. THe default password is `raspberry`
9. **Add your SSH key to the PI** - While in the Pi run `install -d -m 700 ~/.ssh`. On you machine run `cat ~/.ssh/id_rsa.pub | ssh <USERNAME>@<IP-ADDRESS> 'cat >> .ssh/authorized_keys'`

YouTube video of this setup: [https://www.youtube.com/watch?v=Ct9XwyYvmbU](https://www.youtube.com/watch?v=Ct9XwyYvmbU)

**Conneting to the interface:**
At this point, your Pi should be set up and running but you'll need to still connect it to a remotedesktop client to see the interface. 
For this you will need to know the remote IP address of your specific Pi. For this I would suggest downloading the "Lan-Scan" for mac https://apps.apple.com/us/app/lanscan/id472226235?mt=12 - find your pi and copy the IP address given.

<img src=Images/lanscan.png width="500">

Find your pi and look at the IP address, you will use this to connect via a remote desktop client - i'm using VCN viewer https://www.realvnc.com/en/connect/download/viewer/ -

<img src=Images/pidesktop.png width="500">

*in the end, you should have a desktop that looks liek this through your virtual desktop client*

## Libraries and Settings
At this point, we're ready to install everything required for the python script we are going to write to run. All of these command will be done using the terminal.

1. **Update Your Pi** - `sudo apt-get update` `sudo apt-get upgrade` `sudo pip3 install --upgrade setuptools` 
2. **Python3** - `sudo pip install python3`
3. **Tweepy** (python language which interfaces with the Twitter API, I would also suggest twython if tweepy gives you trouble) - `pip install tweepy`
4. **GIPO Library** - `pip3 install RPI.GPIO`
5. **AdafruitBlinkaLibrary** - `pip3 install adafruit-blinka` ( this will allow us to access the preprogramed sequences for the 2605L motor driver)
At this point, I would suggest running the blinka test to make sure the last two packages installed correctly, the test can be found here: https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi
6. **Numpy** - `pip install numpy` (used in sentiment analysis)
7. **pandas** - `pip install pandas` (used in sentiment analysis)
8. **textblob** - `pip install textblob` (used in sentiment analysis)
8. **i2c and SPI configuration** we must enable the i2c connections on the Raspberry Pi as they are set to off by default. Because we will be using the i2c ports to deliver the haptic controls through the motor driver, its necessary to make sure they are connected. There is a great tutorial on how to do this here https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
9. **installing i2c updates** - `sudo apt-get install -y python-smbus` `sudo apt-get install -y i2c-tools` 
10. **enabeling i2c** - `sudo raspi-config` navigate to "interfacting options" > i2c > click yes to "I2C ARM interface enabled" `sudo reboot`

Once you've done all this, you can check in the raspberry pi graphical interface to determine that the changes worked. **Preferences > Configuration > Interfaces**

<img src=Images/changes.png width="500">

## Physical Computing
Connecting the pi to the controler board is quite asy but will require some steps
1. solder pins to the board so that you can connect it to a breadboard
2. solder the vibration motor to the + and  - on the board as directed by your manufacturer
3. connect the the board to the pi using jumper wires as directed below

<img src=Images/Screenshot_2020-06-09_at_20.06.12.png width="500">

After this is all hooked up, you'll want to run a test on the Pi to see that your i2c pins are seeing the controler you have plugged in. Open your terminal and enter the following

```
pi@raspberrypi:~/$ i2cdetect -y 1

```
you should see somehing pop up like this, with at least one set of "00" being repalced with numbers. If you dont see that, it could mean there is a problem with either the physical components, or your i2c settings

<img src=Images/raspi.png width="500">

## Access To Twitter API
You will need to be able to access twitters API in order to create this application. To do so you must register as a developer with twitter -its quite easy and only takes a few minutes. You should be given access within a day or so.

You can register you appliation here https://developer.twitter.com/en
Once it is accepted, you will be give an an API key and Access Token **KEEP THIS SECRET** - its what you will use to access the API through python.

<img src=Images/api.png width="500">

Once you have the Token and Access code, place it in file named "twittercredentials.py" using the format below. Youll then save this into the phtyon environment you create for the project. 

```
# variables that contain the user credentials to access twitter api
ACCESS_TOKEN = " YOUR ACCESS TOKEN GOES HERE"
ACCESS_TOKEN_SECRET = "YOUR ACCESS TOKEN SECRET GOES HERE"
CONSUMER_KEY = "YOUR CONSUMER KEY GOES HERE"
CONSUMER_SECRET = "YOUR CONSUMER SECRET GOES HERE"

```

## Sentiment Analysis
For determining the sentiment analysis of the tweets, were going to be using a python engine called textblob which works by analysing the given words in any tweet. The engine is actually trained by a pattern analyiser as well as an NLTK Classifer which was trained on examining movie reivews. 

The basic premise here is that the textblob engine analysizes the tweets by understanding key words within them, and the order those words appear. This methodology is without doubt flawed, but for experimentaion it works well. 

The sentiment is delviered as a polarity which the user can define. **By default the polarity is returned in a scale form -1 to +1** Within the python script is a function for rounding this up to either a -1,0, or 1 for the tweets. 

I followed an amazing tutorial on how all this comes together here (click image be)

[![click here](https://img.youtube.com/vi/1gQ6uG5Ujiw/0.jpg)](https://www.youtube.com/watch?v=1gQ6uG5Ujiw)

twitter_python/part_5_sentiment_analysis_tweet_data/sentiment_anaylsis_twitter_data.py

## Haptic Controler
We will control the Texas Instruments TI2605L haptic motor contorler by pulling the required libraries into our python scripts. Adafruit thankfully has these pre-packaged as part of their library. If you're using the sparkfun board like me, it will work all the same so dont worry

The controler is preprogramed with 123 unique various haptic sequences. There is a great tutorial on how to set up the controler here https://learn.adafruit.com/adafruit-drv2605-haptic-controller-breakout/python-circuitpython

If you've done everything correctly to date, now would be a good time to test the motor. This script below will run through a sequence of all 123 effects at a rate of one very .5 seconds. 

```
**importing our needed libraries**
import time
import board
import busio
import adafruit_drv2605
 
 
**Initialize I2C bus and DRV2605 module.**
i2c = busio.I2C(board.SCL, board.SDA)
drv = adafruit_drv2605.DRV2605(i2c)

**defining the effect ID from the texas instruments catalogue**
effect_id = 1
while True:
    print("Playing effect #{0}".format(effect_id))
    drv.sequence[0] = adafruit_drv2605.Effect(effect_id)  # Set the effect on slot 0.
    
 ** defining the sequence of the effects**
    drv.play()  # play the effect
    time.sleep(0.5)  # for 0.5 seconds
    drv.stop()  # and then stop (if it's still running)

*setting which effects to start / stop on
    effect_id += 1
    if effect_id > 123:
        effect_id = 1
        
```

<img src=Images/effectid.png width="500">

At this point, when you run the script you should see sequences of start to happen within the haptic motor
<img src=Images/gif.gif width="500">


## Scripts and Execution

Finally we will combine our two python scripts. This was done by using the twitter sentiment analysis script as our base, and copying into it the parts from the vibration script which we were looking for. 

Below here are the new functions added to the sentiment analysis script as well as the new libraries which were added for the haptic motors. I've included a full file in the repository however this should be enough to get you going to play around with what you want the script to do. 

At a very basic level, this script simply reads the sentiment of the given account's last 10 tweets - assigns a polarity- and that polarity assigns an effect ID for the vibration.

```
**libraries coppied over**
import board
import busio
from  adafruit_drv2605 import DRV2605, Effect

**assigning a motor function to correspond with the tweet sentiment**
class Motor (DRV2605): 
    def __init__ (self):
        i2c = busio.I2C(board.SCL, board.SDA)
        super (Motor,self).__init__ (i2c)
    def motorcomand (self, data):
        effect_id=np.abs (data["sentiment"].sum ())
        self.sequence[0] = Effect(effect_id)  # Set the effect on slot 0.
        # You can assign effects to up to 7 different slots to combine
        # them in interesting ways. Index the sequence property with a
        # slot number 0 to 6.
        # Optionally, you can assign a pause to a slot. E.g.
        # drv.sequence[1] = adafruit_drv2605.Pause(0.5)  # Pause for half a second
        self.play()  # play the effect
        time.sleep(0.5)  # for 0.5 seconds
        self.stop()  # and then stop (if it's still running)
```
CLICK ON IMAGE
[![click here](https://img.youtube.com/vi/wXe2I7TXK18/0.jpg)](https://youtu.be/wXe2I7TXK18)

